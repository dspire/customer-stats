<?php

require_once __DIR__ . '/vendor/autoload.php';

$file_path = './data/cdrs.csv';

$countries = new \App\Country();
$customerStats = new \App\CustomerStats();
$ipDetector = new \App\IpService();
$reader = new \App\CsvReader($file_path);
foreach ($reader->getContent() as $record) {
	$customerStatItem = $customerStats->findOrNew($record['customerId']);

	$customerStatItem->addTotalDuration($record['duration']);
	$customerStatItem->increaseTotalNumber();

	if (empty($record['phone']) || empty($record['ipv4'])) {
		continue;
	}

	$info = $ipDetector->setIp($record['ipv4'])->load();
	$continent_by_ip = $info->getContinent()->getCode();

	$country = $countries->searchPossiblePhoneCodes($record['phone'])->first();
	$continent_by_phone = $country['continent_code'];

	if ($continent_by_ip === $continent_by_phone) {
		$customerStatItem->addDuration($record['duration']);
		$customerStatItem->increaseNumber();
	}
}

?>
<!-- render -->