<?php

namespace App;


class Country
{
	public function searchPossiblePhoneCodes($phone_string) {
		$phone_codes = [
			substr($phone_string, 0, 1),
			substr($phone_string, 0, 2),
			substr($phone_string, 0, 3),
			substr($phone_string, 0, 4),
			substr($phone_string, 0, 5),
			substr($phone_string, 0, 6),
		];
		$this->whereIn($phone_codes);

		return $this;
	}

	public function whereIn($values) {
		//todo:
	}

	public function first() {
		return [];
	}
}