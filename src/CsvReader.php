<?php

namespace App;

class CsvReader
{
	private $content;
	private $map = [
		'customerId', 'callAt', 'duration', 'phone', 'ipv4'
	];

	public function __construct(string $file_path) {
		$csv = new \ParseCsv\Csv();
		$csv->fields = $this->map;
		$csv->parse($file_path);

		$this->content = $csv->data;
	}

	public function getContent() {
		return $this->content;
	}
}