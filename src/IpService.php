<?php

namespace App;

use GuzzleHttp\Client;

class IpService
{
	private $client;
	private $key;
	private $ip;
	private $content;

	public function __construct() {
		$this->client = new Client([
			'base_uri' => 'http://api.ipstack.com',
			'timeout' => 2.0,
		]);
		$this->key = '8d8a6b9a981c9cfd0f9955ae0ad0c3a6';
	}

	public function setIp(string $ip) {
		$this->ip = $ip;

		return $this;
	}

	protected function getResponse() {
		$response = $this->client->request(
			'GET',
			$this->ip,
			[
				'query' => [
					'access_key' => $this->key
				]
			]
		);

		if ($response->getStatusCode() != 200) {
			return false;
		}
		$data = $response->getBody()->getContents();

		return json_decode($data);
	}

	public function load() {
		$this->content = $this->getResponse();
		return $this;
	}

	public function getContent() {
		return $this->content;
	}

	public function getContinent(): Continent {
		return new \App\Continent($this->content->continent_code);
	}
}