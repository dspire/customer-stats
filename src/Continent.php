<?php

namespace App;


class Continent
{
	private $code;
	private $name;

	/**
	 * Continent constructor.
	 * @param $code
	 */
	public function __construct(string $code) {
		$this->code = strtoupper($code);
		$this->name = $this->getMapping()[$this->code] ?? '';
	}

	public function getCode(): string {
		return $this->code;
	}

	public function getName(): string {
		return $this->name;
	}

	protected function getMapping() {
		$map = [
			'AF' => 'Africa',
			'AS' => 'Asia',
			'EU' => 'Europe',
			'NA' => 'North America',
			'OC' => 'Oceania',
			'SA' => 'South America',
			'AN' => 'Antarctica',
		];
		return $map;
	}
}